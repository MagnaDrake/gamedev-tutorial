extends Area2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

var screensize
var timer
export var speed = 300
var isShooting = false

onready var bullet = preload("res://Scenes/Laser.tscn")

func _ready():
	screensize = get_viewport_rect().size
	timer = Timer.new()
	timer.set_wait_time(0.2)
	timer.connect("timeout", self, "_on_Timer_timeout")

func _process(delta):
	var velocity = 0
	if Input.is_action_pressed("ui_right"):
		velocity = 1
	if Input.is_action_pressed("ui_left"):
		velocity = -1

	position.x += velocity * speed * delta
	position.x = clamp(position.x, 0, screensize.x)
	
	if Input.is_action_just_pressed("ui_select"):
		var n_bullet = bullet.instance()
		n_bullet.position = position
		n_bullet.position.y -= 30
		get_node("/root/Main").add_child(n_bullet)
		
