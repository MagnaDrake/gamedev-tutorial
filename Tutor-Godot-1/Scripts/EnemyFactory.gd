extends Node2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
var initPosX = 0
var initPosY = 0
var gapX = 75
var gapY = 40
var screensize
onready var enemyPrefab = preload("res://Scenes/Enemy.tscn")

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	screensize = get_viewport_rect().size
	for i in range (50):
		var enemyInstance = enemyPrefab.instance()
		enemyInstance.position = position
		if(initPosX > screensize.x - 50):
			initPosX = 0
			initPosY += gapY
		enemyInstance.position.x += (gapX + initPosX)
		enemyInstance.position.y -= initPosY
		initPosX += gapX
		print (enemyInstance.position.x)
		print (enemyInstance.position.y)
		get_node("/root/Main").call_deferred("add_child", enemyInstance)
	

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass
