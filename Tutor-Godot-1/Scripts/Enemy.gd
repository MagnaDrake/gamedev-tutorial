extends Area2D

var screensize
export var speed = 100
export var forward_step = 40
var isMovingRight
var moving
var padding = 50

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	screensize = get_viewport_rect().size
	isMovingRight = false
	moving = false
	

func _process(delta):
	var velocity = 0
	if (!isMovingRight):
		velocity = -1
	else:
		velocity = 1
		
	position.x += velocity * speed * delta
	
	
	if(position.x < padding or position.x > screensize.x - padding):
		position.y += forward_step
		isMovingRight = !isMovingRight